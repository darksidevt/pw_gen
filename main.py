from pw_gen import Password
from utils import *
new_settings = True
while new_settings:
    print(read_file('menus/logo.txt'))
    pw_length = input('How long should the password be? ')
    print(read_file('menus/password_types.txt'))
    password_types = input('What types of characters should be in the password? ')
    password = Password(int(pw_length), password_types)
    print(f"Your password is: {password.password()}")
    regenerate = True
    while regenerate:
        regenerate = user_confirmation('Would you like to generate another password with the same settings?')
        print(f'Your password is: {password.password()}')
    new_settings = user_confirmation('Would you like to generate a new password with different settings?')
