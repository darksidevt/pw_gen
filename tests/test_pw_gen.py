from pw_gen import Password

def test_uppercase():
    pw = Password(pw_length=16, pw_types='upper').password()
    assert pw.isupper() == True

def test_lowercase():
    pw = Password(pw_length=16, pw_types='lower').password()
    assert pw.islower() == True

def test_numbers():
    pw = Password(pw_length=16, pw_types='number').password()
    assert pw.isnumeric() == True

def test_whitespace():
    pw = Password(pw_length=16, pw_types='upper, lower, number, special').password()
    assert ' ' not in pw

def test_password_length():
    pw = Password(pw_length=16, pw_types='upper,lower,number,special').password()
    assert len(pw) == 16
